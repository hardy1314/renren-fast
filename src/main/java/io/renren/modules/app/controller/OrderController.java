/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.app.controller;


import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.app.annotation.Login;
import io.renren.modules.app.entity.OrderEntity;
import io.renren.modules.app.entity.UserEntity;
import io.renren.modules.app.form.SearchOrderForm;
import io.renren.modules.app.form.UserOrderForm;
import io.renren.modules.app.form.WxLoginForm;
import io.renren.modules.app.service.OrderService;
import io.renren.modules.app.service.UserService;
import io.renren.modules.app.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * 顺序控制器
 *
 * @author jack
 * @date 2022/04/29
 */
@RestController
@RequestMapping("/app/order/")
@Api("订单业务接口")
@Slf4j
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private JwtUtils jwtUtils;

    @Login
    @PostMapping("searchUserOrderList")
    @ApiOperation("查询用户订单")
    public R searchUserOrderList(@RequestBody UserOrderForm form,
                                 @RequestHeader HashMap headers){

        ValidatorUtils.validateEntity(form);

        String token = headers.get("token").toString();
        int userId = Integer.parseInt(jwtUtils.getClaimByToken(token).getSubject());
        int page = form.getPage();
        int length = form.getLength();
        int start = (page - 1)*length;
        HashMap map = new HashMap();
        map.put("userId",userId);
        map.put("start",start);
        map.put("length",length);
        ArrayList<OrderEntity> lis = orderService.searchUserOrderList(map);
        return R.ok().put("list", lis);
    }

    @Login
    @PostMapping("searchOrderById")
    @ApiOperation("查询订单")
    public R searchOrderById(@RequestBody SearchOrderForm form){
        ValidatorUtils.validateEntity(form);
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("id",form.getOrderId());
        OrderEntity orderEntity = orderService.getOne(wrapper);
        return R.ok().put("order", orderEntity);
    }

}
