/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.app.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 支付订单
 *
 * @author Mark sunlightcs@gmail.com
 * @date 2022/04/30
 */
@Data
@ApiModel(value = "收款码付款的表单")
public class ScanCodePayOrderForm {


    @ApiModelProperty(value = "付款码")
    @NotBlank
    @Pattern(regexp = "^1[0-5][0-9]{16}$",message = "不符合正则表达式")
    private String authCode;

    @ApiModelProperty(value = "订单ID")
    @Min(1)
    private Integer orderId;



}
