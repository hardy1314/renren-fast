package io.renren.modules.app.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 用户订单
 *
 * @author jack
 * @date 2022/04/29
 */
@Data
@ApiModel(value = "查询用户订单的表单")
public class UserOrderForm {

    /**
     * 页面
     */
    @ApiModelProperty(value = "页数")
    @NotNull(message = "页数不能为空")
    @Min(1)
    private Integer page;

    /**
     * 长度
     */
    @ApiModelProperty(value = "每页记录数")
    @NotNull(message = "每页记录数不能为空")
    @Range(min = 1,max = 50)
    private Integer length;

}
